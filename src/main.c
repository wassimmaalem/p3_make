
#include <stdio.h>
#include <stdlib.h>
#include <ma.h>
#include <mb.h>
#include <mc.h>
#include <md.h>

int main(int argc, char **argv) {
    printf("Enter make demo app\n");
    return (module_a_init()==EXIT_SUCCESS &&
    		module_b_init()==EXIT_SUCCESS &&
			module_c_init()==EXIT_SUCCESS &&
			module_d_init()==EXIT_SUCCESS &&
			module_a_process()==EXIT_SUCCESS &&
			module_b_process()==EXIT_SUCCESS &&
			module_c_process()==EXIT_SUCCESS &&
			module_d_process()==EXIT_SUCCESS &&
			module_a_close()==EXIT_SUCCESS &&
			module_b_close()==EXIT_SUCCESS &&
			module_c_close()==EXIT_SUCCESS &&
			module_d_close());
}
