/**
Module B
*/


/**
 * init module B
 *
 * \callgraph
 * \return EXIT_SUCCESS if everything is ok or EXIT_FAILURE when error occured
 */
int module_b_init();

/**
 * Close module B
 *
 * free allocated memories
 * \return EXIT_SUCCESS if everything is ok or EXIT_FAILURE when error occured
 */
int module_b_close();

/**
 * processing by module B
 *
 * \return EXIT_SUCCESS if everything is ok or EXIT_FAILURE when error occured
 */
int module_b_process();


