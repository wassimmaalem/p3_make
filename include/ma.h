/**
\image html DrawingHands.jpg "M.Escher: Drawing Hands"
\brief Module A
\details Module A is very important part of project MakeDemo
\author Zoltan (Zamek) Zidarics
\version 1.0
\date 2016
\bug { there are some memory leaking at module_a_init. Bug id #3/2016 }
\copyright GNU Public License
\warning be careful with module_a_init(), because sometimes causes memory leaks.
\defgroup ModuleA Module A

 @{

*/

/*!
  \def MAX(x,y)
  Computes the maximum of \a x and \a y.
*/
/*!
   Computes the absolute value of its argument \a x.
*/
#define ABS(x) (((x)>0)?(x):-(x))

#define MAX(x,y) ((x)>(y)?(x):(y))

#define MIN(x,y) ((x)>(y)?(y):(x))


/**
\enum WeekDays
*/
enum WeekDays { monday=0, tuesday, wednesday, thirsday, friday, saturday, sunday };


/**
 * init module A
 *
 *\todo find the bug of module_a_init()
 *
 * \callgraph
 * \callergraph
 * \return EXIT_SUCCESS if everything is ok or EXIT_FAILURE when error occured
 */
int module_a_init();

/**
 * Close module A
 *
 * \todo check memory allocation!
 *
 * free allocated memories
 * \return EXIT_SUCCESS if everything is ok or EXIT_FAILURE when error occured
 */
int module_a_close();

/**
 * processing by module A
 *
 * Example:
 * \code{.c}

 *
 *   int result = module_a_process();
 *   if (result != EXIT_SUCCESS)
 *   	abort();
 *
 * \endcode
 *
 * \return EXIT_SUCCESS if everything is ok or EXIT_FAILURE when error occured
 */
int module_a_process();


/**
 * Old style processing of Module A
 * Please do not use it in a new project!
 * \deprecated { plese use #module_a_process() instead of this }
 * \return EXIT_SUCCESS if everything is ok or EXIT_FAILURE when error occured
 */
int module_a_oldstyle_process();


/**
 * End of Module A
 * @}
 */
