BIN=bin
SRC=src
CFLAGS= -Wall -g -pedantic --std=c11 -D_DEFAULT_SOURCE

OBJS=$(patsubst $(SRC)/%.c,$(BIN)/.o,$(wildcard $(SRC)/*.c))
$(BIN)/mm :$(BIN) $(OBJS)
	$(CC) -o $(BIN)/mm $(OBJS) $(CFLAGS)
$(BIN)/%.o :$(SRC)/%.c
	$(CC) -c $< -o $@ $(CFLAGS)
$(BIN):
	mkdir $(BIN)
clean:
	$(RM) -r $(BIN)
all: $(BIN)/person
.PHONY: clean all
